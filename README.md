# JavaScript Assignments
Assignments are located [here](./assignments.md)

# Getting Started
- Clone this repo
- Install dependencies: `npm install`
- Run the app: `npm start`
- Build & Deploy: `./devops/build-deploy-bat`

# Architecture
![](./docs/gs_arch.png)

# S3 Deployment
- Enable website hosting
- Add index.html to properties
- Add the following bucket policy:

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::<BUCKET NAME>/*"
        },
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::<AWS ACCOUNT NUMBER NO DASHES>:user/<AWS USERNAME>"
            },
            "Action": "*",
            "Resource": "arn:aws:s3:::<BUCKET NAME>/*"
        },
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::<AWS ACCOUNT NUMBER NO DASHES>:user/<AWS USERNAME>"
            },
            "Action": "*",
            "Resource": "arn:aws:s3:::<BUCKET NAME>"
        }
    ]
}
```

# Images
[Dynamically Loading](https://www.upbeatcode.com/react/where-to-store-images-in-react-app/)

# Notes
- [React Router](https://reactrouter.com/docs/en/v6/getting-started/tutorial)
- [React Router Video](https://youtu.be/Law7wfdg_ls)
- [UseEffect](https://stackoverflow.com/questions/53219113/where-can-i-make-api-call-with-hooks-in-react)
- [UseEffect Video](https://www.youtube.com/watch?v=k0WnY0Hqe5c)

