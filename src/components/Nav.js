import React from 'react';
import '../css/nav.css';
import { Link } from 'react-router-dom';

export const Nav = (props) => {

    const handleSearch = () => {
        console.log('Searching...');
    }

    return (
        <div className="nav">
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="container-fluid">
                    <Link className="navbar-brand" to='/'>The Guitar Place</Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <Link className="nav-link active" to='/customshop'>Custom Shop</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link active" to='/account'>Account</Link>
                            </li>
                            <li className="nav-item nav-link active">
                                <span className="nav__cart" onClick={props.onShowCart}>Cart</span>
                            </li>
                        </ul>
                        <form className="d-flex">
                            <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                        </form> 
                        <button className="btn btn-secondary" onClick={handleSearch}>Search</button>
                    </div>
                </div>
            </nav>
        </div>
    );
};
