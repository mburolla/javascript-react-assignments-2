import React from 'react';
import '../css/cart.css';

export const Cart = (props) => {

  const handleCheckOut = (guitarIds) => {
    console.log('Purchase: ' + guitarIds)
  }

  if (!props.show) {
    return null;
  }

  return (
    <div className="cart">
        Cart
        <br />
        <hr/>
        {
          props.purchasedGuitars.map(gid => 
            <div key={gid}>
              {gid}
              &nbsp;<button className="cart__btn_remove" onClick={() => props.onRemoveFromCart(gid)}>-</button>
              <br/>
            </div>
          )
        }
        <hr />
        <button className="btn btn-secondary" onClick={() => handleCheckOut(props.purchasedGuitars)}>Check Out</button>
    </div>
  );
};
