import React from 'react';
import { Card } from './Card';
import '../css/guitars.css';

export const Guitars = (props) => {
    return (
        <div className="guitars">
            <div>
                <div className="container-md">
                    <div className="row">
                        {
                            props.guitars.map(i =>
                                <div key={i} className="col-lg-3"> 
                                    <Card onBuy={(gid) => props.onBuy(gid)} guitarId={i} purchasedGuitars={props.purchasedGuitars}/>
                                </div>
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    );
};
