import React from 'react';
import { useState, useEffect } from 'react';
import { getPictureOfTheDay } from '../proxies/apiProxy';
import '../css/index.css';

export const Index = () => {
    let [picture, setPicture] = useState(null);
    
    // Fires once only after the component has been mounted into the DOM (after browser refresh).
    useEffect(() => {
        const fetchPicture = async () => { 
            setPicture(await getPictureOfTheDay());
        }
        fetchPicture();
    }, []); // Forgetting the [] results in an infinite loop!

    // The first time invoking this function, picture will be null.
    // Then useEffect fires once, sets the state, which forces
    // a re-render.
    if (picture == null) {
        return <div className="index__loading">Loading...</div>;
    }

    return (
        <div className="index">
            {picture && <img alt="" src={picture.url}></img>} 
            <br />
            {picture && picture.title} 
            <br />
            {picture && picture.explanation} 
            <br />
            (X-RateLimit-Remaining: {picture && picture.rateLimitRemaining}) 
        </div>
    );
};
