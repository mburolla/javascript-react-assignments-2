import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Nav } from "./Nav";
import { Guitars } from './Guitars';
import { CustomShop } from "./CustomShop";
import { Account } from "./Account";
import { Cart } from "./Cart";
import { useState, useEffect } from 'react';


const pkg = require('../../package.json');

function App() {
  let [showCart, setShowCart] = useState(true);
  let [allGuitars, setAllGuitars] = useState([]);
  let [purchasedGuitars, setPurchasedGuitars] = useState([]);

  useEffect(() => {
    console.log(pkg.version)
    setAllGuitars([1,2,3,4,5,6,7]); 
  }, []); 

  return (
    <div className="app">
      <table>
        <tbody>
            <tr>
              <td>
                <BrowserRouter>
                  <Nav className="nav" onShowCart={() => setShowCart(!showCart)}/>
                  <Routes>
                    <Route path="/" element={<Guitars guitars={allGuitars} purchasedGuitars={purchasedGuitars} onBuy={(gid) => setPurchasedGuitars([...purchasedGuitars, gid])}/>}/>
                    <Route path="customshop" element={<CustomShop />}/>
                    <Route path="account" element={<Account />}/>
                    {/* <Route authed="false" path='/child2' component={PrivateRoute} /> */}
                  </Routes>
                </BrowserRouter>
              </td>
              <td>
                <Cart 
                  className="cart"
                  show={showCart}
                  purchasedGuitars={purchasedGuitars}           
                  onRemoveFromCart={(gid) => setPurchasedGuitars(purchasedGuitars.filter(i => i !== gid))}
                 />
              </td>
            </tr>
          </tbody>
      </table>
    </div>
  );
}

export default App;
