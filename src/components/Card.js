import React from 'react';
import { useState, useEffect } from 'react';
import '../css/card.css';

export const Card = (props) => {
    let [isDisable, setIsDisabled] = useState(false);

    useEffect(() => {
        if (props.purchasedGuitars.filter(i => i === props.guitarId).length > 0) {
            setIsDisabled(true)
        }
        else {
            setIsDisabled(false)
        }
    }, [props.purchasedGuitars, props.guitarId]); 

    return (
        <div className="card">
            Suhr Modern
            <div className="card__picture">
                <img className="img-fluid" src={require('../assets/gtr-1.jpg')} width="190px" alt=""/>
            </div>
            <div className="card__buttons">
                <button>Specs</button>
                <button disabled={isDisable} onClick={() => props.onBuy(props.guitarId)}>Buy</button>
            </div>
        </div>
    );
};