import axios from "axios";

const API_KEY = '7lmIx7rJ9gVYUcQybBVyn0JDdUCafXSB7K7HegT5';

//
// GET: https://api.nasa.gov/planetary/apod?api_key=7lmIx7rJ9gVYUcQybBVyn0JDdUCafXSB7K7HegT5
//

export const getPictureOfTheDay = async () => {
    try {
        let res = await axios.get(`https://api.nasa.gov/planetary/apod?api_key=${API_KEY}`);
        res.data.rateLimitRemaining = res.headers['x-ratelimit-remaining'];
        return res.data;
    }
    catch(err) {
        console.log(err);
        return '';
    }
};
