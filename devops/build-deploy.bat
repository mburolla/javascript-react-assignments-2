@echo off
echo *******************************************************
echo ********************  Building  ***********************
echo *******************************************************
echo: 

call npm run build

echo:
echo *******************************************************
echo *******************  Versioning  **********************
echo *******************************************************
echo:  

call npm version minor > temp.txt

for /f "tokens=*" %%s in (temp.txt) do (
    echo %%s
)

echo:
echo *******************************************************
echo ***************  Deploying to S3  *********************
echo *******************************************************
echo: 

aws s3 sync ../build s3://js-react-2 

echo:
echo *******************************************************
echo *****************  Git Tagging  ***********************
echo *******************************************************
echo:  

for /f "tokens=*" %%s in (temp.txt) do (
    echo %%s
    git tag -a %%s -m %%s
    git push origin %%s
)

echo:  
echo *******************************************************
echo ******************  Try it out  ***********************
echo *******************************************************
echo: 
echo http://js-react-2.s3-website-us-east-1.amazonaws.com/
echo: 
